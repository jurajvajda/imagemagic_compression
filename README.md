# imagemagic compression #

Compress JPG images for the web using imagemagic mogrify. This is for Linux like systems script only. Adjusting the script a bit can compress also PNG and maybe others. This script is intended for bringing automation to a website development.

### How do I get set up? ###

* add script to the folder where you have all your images to compress
* run ./optimize_images.sh
* this will create 'compressed' folder with a copy of compressed images from your folder

### Links ###
* [mogrify documentation](http://www.imagemagick.org/script/mogrify.php)

### License ###
WTFPL